package com.freightify;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * Unit test for hashtree
 */
public class AppTest {

    private List<IConfiguration<String>> getConfigList(ArrayList<String> keys) {
        List<IConfiguration<String>> configKeys = new ArrayList<>();
        Configuration<String> cr;

        for (int i = 0; i < keys.size(); i++) {
            String[] ka = keys.get(i).split("=");
            cr = new Configuration<>(ka[0], ka[1]);
            configKeys.add(cr);
        }
        return configKeys;
    }

    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithRelativeMatch() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e=1");
        keys.add("a.b.c=2");
        keys.add("a.*.c=3");

        HashTree<String> tree = new HashTree<>(getConfigList(keys), ".");

        String keyToFind = "a.f.c";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("3"));

        keyToFind = "a.b.c";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("2"));

        keyToFind = "a.b.e";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("1"));
    }

    @Test
    public void shouldAnsweWithRelativeMatch2() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b=1");
        keys.add("a.*.c.h.*=2");
        keys.add("a.*.*.d.g=3");
        keys.add("a.*.*.d.*=4");
        keys.add("a.*.*.*.e=5");
        keys.add("a.*.*.*.f=6");

        HashTree<String> tree = new HashTree<>(getConfigList(keys), ".");

        String keyToFind = "a.b.c.h.g";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("2"));

        keyToFind = "a.f.c.h.g";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("2"));

        keyToFind = "a.f.v.h.g";
        value = tree.priorityGet(keyToFind);
        assertTrue(value == null);

        keyToFind = "a.f.v.h.f";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("6"));
    }

    @Test
    public void shouldAnsweWithRelativeMatch3() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.*.c.h.*=1");

        HashTree<String> tree = new HashTree<>(getConfigList(keys), ".");

        String keyToFind = "a.b.c.h.g";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("1"));

        keyToFind = "a.f.c.h.g";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("1"));
    }

    @Test
    public void shouldAnsweWithRealWordValues() {

        ArrayList<String> keys = new ArrayList<String>();
        keys.add("211.168.l2.20GP.*.*.*.IN.*=1");
        keys.add("211.168.l2.40GP.*.*.*.IN.*=2");
        keys.add("211.168.l2.20GP.s1.*.*.IN.*=3");
        keys.add("211.168.l2.20GP.s1.fak.*.IN.*=4");
        keys.add("211.168.l2.20GP.s1.fak.rice.IN.CN=5");
        keys.add("211.168.l2.20GP.s2.*.*.IN.*=6");

        keys.add("211.168.l4.20GP.*.*.*.IN.*=7");
        keys.add("211.168.l4.40GP.s3.*.*.IN.*=8");

        keys.add("211.169.l2.20GP.*.*.*.IN.*=9");
        keys.add("212.169.l1.20GP.*.*.*.IN.*=10");
        keys.add("211.169.l1.20GP.*.*.*.IN.*=11");
        keys.add("212.167.l1.20GP.*.*.*.IN.*=12");
        keys.add("211.167.l1.20GP.*.*.*.IN.*=13");
        keys.add("212.167.l2.40GP.*.*.*.CN.*=14");
        keys.add("213.168.l4.20GP.*.*.*.IN.*=15");

        HashTree<String> tree = new HashTree<>(getConfigList(keys), ".");

        String keyToFind = "211.169.l2.20GP.*.*.*.IN.*";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("9"));

        keyToFind = "211.168.l2.20GP.*.*.*.IN.*";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("1"));

        keyToFind = "211.168.l4.20GP.*.*.*.CN.*";
        value = tree.priorityGet(keyToFind);
        assertTrue(value == null);

        keyToFind = "213.168.l4.20GP.*.*.*.CN.*";
        value = tree.priorityGet(keyToFind);
        assertTrue(value == null);

        keyToFind = "213.168.l4.20GP.*.*.*.IN.*";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("15"));

        keyToFind = "211.168.l2.20GP.s1.fak.*.IN.*";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("4"));

        keyToFind = "211.168.l2.20GP.s2.fak.*.IN.*";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("6"));

        keyToFind = "211.168.l2.20GP.s1.ak47.*.IN.*";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("3"));

        keyToFind = "211.168.l3.20GP.s1.ak47.*.IN.*";
        value = tree.priorityGet(keyToFind);
        assertTrue(value == null);
    }

    @Test
    public void shouldAnswerWithNullWhenPartialKey() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e=1");
        keys.add("a.b.c=2");
        keys.add("a.*.c=3");

        HashTree<String> tree = new HashTree<>(getConfigList(keys), ".");

        String keyToFind = "a.f";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value == null);
    }

    @Test
    public void shouldAnswerWithNullWhenEmptyKey() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e=1");
        keys.add("a.b.c=2");
        keys.add("a.*.c=3");
        HashTree<String> tree = new HashTree<>(getConfigList(keys), ".");

        String keyToFind = "";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value == null);
    }

    @Test
    public void shouldAnswerWithNullWhenExtraKey() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e=1");
        keys.add("a.b.c=2");
        keys.add("a.*.c=3");
        HashTree<String> tree = new HashTree<>(getConfigList(keys), ".");

        String keyToFind = "a.b.c.*";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value == null);
    }

    @Test
    public void shouldAnswerWithLatestValueWhenDupDataEntered() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e=1");
        keys.add("a.b.e=2");
        keys.add("a.*.e=3");

        HashTree<String> tree = new HashTree<>(getConfigList(keys), ".");

        String keyToFind = "a.b.e";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("2"));
    }

    @Test
    public void shouldAnswerWithGenericValueWhenAllGenericDataIsPresent() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e=1");
        keys.add("a.b.e=2");
        keys.add("a.*.e=3");
        keys.add("*.*.*=4");

        HashTree<String> tree = new HashTree<>(getConfigList(keys), ".");

        String keyToFind = "*.*.*";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("4"));

        keyToFind = "x.y.z";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("4"));
    }

    @Test
    public void shouldAnswerWithRelativeWhenDifferentDelimeterIsPassed() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a_b_e=1");
        keys.add("a_b_e=2");
        keys.add("a_*_e=3");
        keys.add("*_*_*=4");

        String delimeter = "_";
        HashTree<String> tree = new HashTree<>(getConfigList(keys), delimeter);

        String keyToFind = "*_*_*";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("4"));

    }

    @Test
    public void shouldSetKey() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a_b_e=1");
        keys.add("a_b_e=2");
        keys.add("a_*_e=3");
        keys.add("*_*_*=4");

        List<IConfiguration<String>> configKeys = new ArrayList<>();
        Configuration<String> cr;

        for (int i = 0; i < keys.size(); i++) {
            String[] ka = keys.get(i).split("=");
            cr = new Configuration<>(ka[0], ka[1]);
            configKeys.add(cr);
        }
        String delimeter = "_";
        HashTree<String> tree = new HashTree<>(configKeys, delimeter);

        cr = new Configuration<>("x_y_z", "5");
        Boolean overwrite = false;
        tree.set(cr, delimeter, overwrite);

        String keyToFind = "x_y_z";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("5"));

        // tree.levelTraversal();

    }

    @Test
    public void shouldSetKey2() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e=1");

        List<IConfiguration<String>> configKeys = new ArrayList<>();
        Configuration<String> cr;

        for (int i = 0; i < keys.size(); i++) {
            String[] ka = keys.get(i).split("=");
            cr = new Configuration<>(ka[0], ka[1]);
            configKeys.add(cr);
        }
        String delimeter = "_";
        HashTree<String> tree = new HashTree<>(configKeys, delimeter);

        cr = new Configuration<>("a.*.e", "2");
        Boolean overwrite = false;
        tree.set(cr, delimeter, overwrite);

        String keyToFind = "a.*.e";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("2"));
    }

    @Test
    public void shouldOverwriteTree() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e=1");
        keys.add("*.b.e=2");
        keys.add("a.v.e=3");
        keys.add("x.v.g=4");

        List<IConfiguration<String>> configKeys = new ArrayList<>();
        Configuration<String> cr;

        for (int i = 0; i < keys.size(); i++) {
            String[] ka = keys.get(i).split("=");
            cr = new Configuration<>(ka[0], ka[1]);
            configKeys.add(cr);
        }
        String delimeter = ".";
        HashTree<String> tree = new HashTree<>(configKeys, delimeter);

        cr = new Configuration<>("a.*.e", "2");
        Boolean overwrite = true;
        tree.set(cr, delimeter, overwrite);

        String keyToFind = "x.v.g";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value == null);

        keyToFind = "a.*.e";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("2"));

        keyToFind = "a.c.f";
        value = tree.priorityGet(keyToFind);
        assertTrue(value == null);

        cr = new Configuration<>("x.*.e", "2");
        overwrite = true;
        tree.set(cr, delimeter, overwrite);

        keyToFind = "x.v.e";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("2"));

        keyToFind = "a.b.e";
        value = tree.priorityGet(keyToFind);
        assertTrue(value == null);

    }

    @Test
    public void shouldCreateTreeUsingSet() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e=1");

        List<IConfiguration<String>> configKeys = new ArrayList<>();
        Configuration<String> cr;

        for (int i = 0; i < keys.size(); i++) {
            String[] ka = keys.get(i).split("=");
            cr = new Configuration<>(ka[0], ka[1]);
            configKeys.add(cr);
        }
        String delimeter = ".";
        HashTree<String> tree = new HashTree<>(configKeys, delimeter);

        cr = new Configuration<>("z.*.e", "2");
        Boolean overwrite = true;
        tree.set(cr, delimeter, overwrite);

        overwrite = false;
        cr = new Configuration<>("x.*.e", "3");
        tree.set(cr, delimeter, overwrite);

        cr = new Configuration<>("y.c.v", "4");
        tree.set(cr, delimeter, overwrite);

        // tree.levelTraversal();

        String keyToFind = "a.b.e";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value == null);

        keyToFind = "x.*.e";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("3"));

        keyToFind = "y.c.v";
        value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("4"));

    }

    @Test
    public void shouldRemoveKeyFromTree() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e=1");
        HashTree<String> tree = new HashTree<>(getConfigList(keys), ".");
        tree.levelTraversal();
        tree.remove("a.b.e", false);
        System.out.println("after remove");
        tree.levelTraversal();
        String keyToFind = "a.b.e";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value == null);
    }

    @Test
    public void shouldNotRemoveKeyFromTreeIfNotPresent() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e=1");
        HashTree<String> tree = new HashTree<>(getConfigList(keys), ".");
        Boolean isRemoved = tree.remove("x.b.e", false);
        String keyToFind = "a.b.e";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value.equals("1"));

        assertTrue("not removed", isRemoved == false);
    }

    @Test
    public void shouldRemoveLeafKey() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e=1");
        keys.add("a.b.f=2");
        keys.add("a.b.g=3");

        HashTree<String> tree = new HashTree<>(getConfigList(keys), ".");
        tree.levelTraversal();
        Boolean isRemoved = tree.remove("a.b.e", false);
        System.out.println("after remove");
        tree.levelTraversal();
        String keyToFind = "a.b.e";
        String value = tree.priorityGet(keyToFind);
        System.out.println("value is " + value);
        assertTrue(value == null);

        assertTrue("leaf key removed", isRemoved == true);
    }

    @Test
    public void shouldRemoveMultipleLeafKeys() {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e.h.i.j.k=1");
        keys.add("x.b.e.h.i.j.k=2");
        keys.add("y.b.e.h.i.j.k=3");
        keys.add("a.b.v.b.n.l.m=4");

        HashTree<String> tree = new HashTree<>(getConfigList(keys), ".");
        tree.levelTraversal();
        // should remove all keys after b
        Boolean isRemoved = tree.remove("a.b.e.h.i.j.k", false);
        // System.out.println("after remove");
        // tree.levelTraversal();
        String keyToFind = "a.b.e.h.i.j.k";
        String value = tree.priorityGet(keyToFind);
        assertTrue(value == null);
        assertTrue("leaf key removed", isRemoved == true);

        // should return other part of the tree
        keyToFind = "a.b.v.b.n.l.m";
        value = tree.priorityGet(keyToFind);
        System.out.println("value is " + value);
        assertTrue(value.equals("4"));
    }
}