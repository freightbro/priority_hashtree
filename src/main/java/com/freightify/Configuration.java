package com.freightify;

public class Configuration<T> implements IConfiguration<T>{

    private String key;

    private T value;

    Configuration(String key, T value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String getKey() {
        return this.key;
    }

    @Override
    public T getValue() {
        return this.value;
    }

    
}
