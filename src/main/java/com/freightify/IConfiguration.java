package com.freightify;

public interface IConfiguration<T> {
    
    String getKey();

    T getValue();

}
