package com.freightify;

import java.util.*;

/**
 * Hashtree data structure implementation
 * 
 * @author Sumit Bopche
 */
public class HashTree<T> {
    Node root;
    String delimeter;
    String wildKey = "*";
    HashMap<String, T> configMap = new HashMap<>();

    class Node {
        String data;
        HashMap<String, Node> childrens = new HashMap<>();

        Node(String data) {
            this.data = data;
            this.childrens = new HashMap<>();
        }
    }

    HashTree(List<IConfiguration<T>> configs, String delimiter) {
        this.root = new Node("root");
        this.delimeter = delimiter;
        this.constructHashTree(configs, delimiter);
    }

    /**
     * 
     * @param root
     * @param configList
     * @param delimeter
     */
    private void constructHashTree(List<IConfiguration<T>> configList, String delimeter) {
        if (delimeter.equals(".")) {
            delimeter = "\\" + delimeter;
        }

        for (int i = 0; i < configList.size(); i++) {
            this.setKey(configList.get(i), delimeter);
        }
    }

    /**
     * Set key in the existing hashtree. This appends key to root node
     * 
     * @param config
     * @param delimeter
     */
    private void setKey(IConfiguration<T> config, String delimeter) {
        Node parent = this.root;
        String[] localKeysList = config.getKey().split(delimeter);
        this.configMap.put(config.getKey(), config.getValue());

        for (int j = 0; j < localKeysList.length; j++) {
            if (!parent.childrens.containsKey(localKeysList[j])) {
                Node newChild = new Node(localKeysList[j]);
                parent.childrens.put(newChild.data, newChild);
                parent = newChild;
            } else {
                parent = parent.childrens.get(localKeysList[j]);
            }
        }
    }

    /**
     * This appends the new key in existing hash tree or overwrites the entire tree
     * if overwrite is true
     * 
     * @param config
     * @param delimeter
     * @param overwrite
     */
    public Boolean set(IConfiguration<T> config, String delimeter, Boolean overwrite) {
        if (delimeter.equals(".")) {
            delimeter = "\\" + delimeter;
        }

        if (overwrite == true) {
            this.root = new Node("root");
        }

        this.setKey(config, delimeter);
        return true;
    }

    /**
     * Deletes the key from hashtree and configMap
     * 
     * @param keyToDelete
     * @param removeAll
     */
    public Boolean remove(String keyToDelete, Boolean removeAll) {

        // removes all the keys
        if (removeAll) {
            this.root = new Node("root");
            return true;
        }

        if (this.configMap.containsKey(keyToDelete)) {
            this.configMap.remove(keyToDelete);
            String delimeter = this.delimeter;
            if (this.delimeter.equals(".")) {
                delimeter = "\\" + this.delimeter;
            }

            Node parent = this.root;
            String[] localKeys = keyToDelete.split(delimeter);
            Stack<Node> nodeStack = new Stack<>();
            nodeStack.add(parent); // adding root in node stack
            for (int i = 0; i < localKeys.length; i++) {
                if (parent.childrens.containsKey(localKeys[i])) {
                    parent = parent.childrens.get(localKeys[i]);
                    nodeStack.add(parent);
                }
            }

            Boolean childIsLeafNode = false;
            String currentKeyToDelete = "";

            /**
             * Here removing nodes one by one, starting from leaf node.
             * Once we remove last key, we check it's immediate parent if don't have any
             * children
             * then we remove it too.
             * At some point if parent having more than 0 childrens even
             */
            while (nodeStack.size() > 0) { // as last element is root
                Node topNode = nodeStack.pop();
                if (childIsLeafNode == true) {
                    topNode.childrens.remove(currentKeyToDelete);
                }
                if (topNode.childrens.size() == 0) {
                    currentKeyToDelete = topNode.data;
                    childIsLeafNode = true;
                } else {
                    // if there are multiple childrens then we can not remove that node
                    childIsLeafNode = false;
                    break;
                }
            }
        } else {
            System.out.println("key not available in tree");
            return false;
        }

        return true;
    }

    /**
     * Prints the tree level by level (BFS)
     * This is for getting the idea about how tree is built.
     */
    public void levelTraversal() {
        HashMap<String, Node> queue = this.root.childrens;
        while (queue.size() > 0) {
            HashMap<String, Node> localQ = new HashMap<>();
            for (Map.Entry<String, Node> q : queue.entrySet()) {
                System.out.println(q.getKey());
                System.out.println(q.getValue().childrens);
                localQ.putAll(q.getValue().childrens);
            }
            System.out.println("-------------");
            queue = localQ;
        }
    }

    /**
     * Find the exact key if available else find closest matching key with key
     * received in request.
     * 
     * @param keyToFind
     * @return value of the relative key
     */
    public T priorityGet(String keyToFind) {
        String delimeter = this.delimeter;
        if (delimeter.equals(".")) {
            delimeter = "\\" + delimeter;
        }

        String[] keyParts = keyToFind.split(delimeter);
        Stack<Node> nodeStack = new Stack<>();
        HashMap<Node, Boolean> wrongPathNodes = new HashMap<>();

        Node currentParent = this.root;
        nodeStack.push(currentParent);
        int kpIndex = 0;
        while (nodeStack.size() > 0 && nodeStack.size() <= keyParts.length) {
            currentParent = nodeStack.peek();
            Boolean exactFound = false, genericFound = false;
            if (currentParent.childrens.containsKey(keyParts[kpIndex])
                    && !wrongPathNodes.containsKey(currentParent.childrens.get(keyParts[kpIndex]))) {
                currentParent = currentParent.childrens.get(keyParts[kpIndex]);
                exactFound = true;
            } else if (currentParent.childrens.containsKey(wildKey)
                    && !wrongPathNodes.containsKey(currentParent.childrens.get(wildKey))) {
                currentParent = currentParent.childrens.get(wildKey);
                genericFound = true;
            }

            if (exactFound == true || genericFound == true) {
                nodeStack.push(currentParent);
                kpIndex += 1;
            } else {
                Node nodeNotToVisitAgain = nodeStack.pop();
                wrongPathNodes.put(nodeNotToVisitAgain, true);
                kpIndex -= 1;
            }
        }
        String result = "";
        if (nodeStack.size() > 1) {
            result += nodeStack.get(1).data;
        }

        for (int i = 2; i < nodeStack.size(); i++) {
            result += this.delimeter + nodeStack.get(i).data;
        }

        return this.configMap.get(result);
    }

}
