package com.freightify;

import java.util.*;

public class App {
    public static void main(String[] args) {
        ArrayList<String> keys = new ArrayList<String>();
        keys.add("a.b.e");
        keys.add("a.b.c");
        keys.add("a.*.c");
        keys.add("b.v.c");
        keys.add("c.g.c");
        keys.add("d.h.c");

        List<IConfiguration<String>> configKeys = new ArrayList<IConfiguration<String>>();
        Configuration<String> cr;

        for (int i = 0; i < keys.size(); i++) {
            cr = new Configuration<String>(keys.get(i), keys.get(i).replace(".", ""));
            configKeys.add(cr);
        }

        System.out.println(configKeys.size());

        HashTree<String> tree = new HashTree<>(configKeys, ".");

        String keyToFind = "a.f.c";
        String closestAvailableKey = tree.priorityGet(keyToFind);

        System.out.println("Closest Available Key " + closestAvailableKey);

    }
}
