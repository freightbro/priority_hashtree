# Priority Hashtree

Custom Hashtree which accepts the key and returns value of exact or closest matching key.

## Key

Key can be created by combining multiple parameters using some delimeters like "." or "_"

Example: \
name: vp \
code" 5 \
service: s1 \
isActive: true 

Respective key will be "vp.5.s1.true"

Use wildcard character "*" in key if particular params value can be anything.

Example:
if we don't care about the value of service and isActive then \
Respective key should be "vp.5.\*.*"

### Constraints
* Key : String
* Value: Any Type

## Behaviour of Priority Hashtree

Initialising tree with following data.

| Configured Key | Value |
|----------------|-------|
|                |       |
| a.b.c          |     1 |
| a.\*.c          |     2 |
| a.\*.*          |     3 |
| \*.\*.*          |     4 |
| s.d.v          |     5 |
| s.\*.*          |     6 |
| *.r.t          |     7 |


Values to be expected when we pass particular key in priority get function call. 

| Requested Key In Priority Get | Value To Be Expected |
|-------------------------------|----------------------|
|                               |                      |
| a.b.c                         |                    1 |
| a.x.c                         |                    2 |
| a.y.c                         |                    2 |
| a.g.h                         |                    3 |
| a.f.d                         |                    3 |
| x.y.z                         |                    4 |
| s.y.z                         |                    6 |
| s.d.v                         |                    5 |
| s.\*.z                         |                    6 |
| \*.\*.*                         |                    4 |
| s.\*.v                         |                    6 |
| h.r.t                         |                    7 |
| *.r.t                         |                    7 |
| \*.*.t                         |                    4 |


## Installation

### Gradle

Gradle Groovy DSL install command

```
implementation 'com.gitlab.freightbro:priority_hashtree:v1.0.0'
```

Add Gradle Groovy DSL repository command

```
maven {
  url 'https://gitlab.com/api/v4/projects/33935828/packages/maven'
}
```

Run build command

```
./gradlew clean build
```
or
```
./gradlew clean build -x test
```

This will install the library in your gradle project

## How to use it

HashTree constructor expects List<IConfiguration<String>> and a delimeter.

Create IConfiguration interface.
```java
package yourpackage;

public interface IConfiguration<T> {
    
    String getKey();

    T getValue();

}
```

and Configuration class which will implement IConfiguration

```java
package yourpackage;

public class Configuration<T> implements IConfiguration<T>{
    private String key;
    private T value;

    Configuration(String key, T value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String getKey() {
        return this.key;
    }

    @Override
    public T getValue() {
        return this.value;
    }
}
```

Now create the list of the configuration.\
Example.
```java
List<IConfiguration<String>> configKeys = new ArrayList<>();
Configuration<String> cr;

cr = new Configuration<>("a.b.c", 1); // here value can be of any type
configKeys.add(cr)
cr = new Configuration<>("a.*.c", 1);
configKeys.add(cr)

```

Create hashtree

```java
String delimeter = ".";
HashTree<String> tree = new HashTree<>(configKeys, delimeter); // here HashTree datatype is String but you can change it as per the data type of your value.
```

### Priority Get
Get Value from Priority Get Function

```java
String value = tree.priorityGet("a.b.c"); // will return value 1 as per the configuration done above.
```

### Set Key

```java
//The key value which you want you insert in tree
cr = new Configuration<>("x_y_z", "5");
String delimeter = "_";
Boolean overwrite = false; // set it true if you want overwrite the entire tree with new key
Boolean isSet = tree.set(cr, delimeter, overwrite);
```

set function will append the key in existing tree. If overwrite param is true then it will replace the entire tree with new key.

### Remove key

```java
    Boolean removeAll = false;
    Boolean isRemoved = tree.remove("a.b.c", removeAll);
```

This will remove the specified key from tree. If removeAll key is set true then all the keys in tree will be removed.

